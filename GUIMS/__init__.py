import os
from flask import Flask

def create_app(test_config=None):
    app = Flask(_name_, instance_relative_config=True)
    app.jinja_env.add_extension("jinja2.ext.loopcontrols")

    if(test_config is None):
        app.config.from_pyfile("config.py", silent=True)
    else:
        app.config.from_mapping(test_config)

    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass

    return app